package tk.baumi.jaraco;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Stack;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * The folder-activity shows a folder of the server in a listview.<br/>
 * The user can go deeper or higher in the file-system.<br/>
 * Finally he or she is able to select a file that shoule be added to the playlist.
 * @author Manuel Baumgartner
 *
 */
public class FolderActivity extends Activity{
	
	public static ListView lvFolder;
	public static FolderAdapter aaFolder;
	public static ArrayList<String> alFolder;
	Stack<Integer> fPosition;
	public static int lPosition = -1;
	String folder = "";
	static int select = 0;
	//private IPEncryption ipEn;
	private StateTransfer.FolderListener folderListener;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.folderview);
		
		//ipEn = new IPEncryption();
		lvFolder = (ListView)findViewById(R.id.lvFolder);
		lvFolder.setCacheColorHint(0);
		lvFolder.setFastScrollEnabled(true);
		alFolder = new ArrayList<String>();
		folderListener = new StateTransfer.FolderListener() {
			
			@Override
			public void getFolder(final ArrayList<String> list) {
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							FolderActivity.alFolder.clear();
							alFolder.addAll(list);
							FolderActivity.select = -1;
							FolderActivity.aaFolder.notifyDataSetChanged();
							FolderActivity.lvFolder.setSelection(FolderActivity.lPosition);
						}
					});
			}
			
			@Override
			public void extendFolder(final ArrayList<String> list) {
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						alFolder.addAll(list);
						FolderActivity.select = -1;
						FolderActivity.aaFolder.notifyDataSetChanged();
						FolderActivity.lvFolder.setSelection(FolderActivity.lPosition);
						
					}
				});
			}
		};
		Jaraco.transfer.addFolderListener(folderListener);
		
		try {
			folder = "\\";
			byte[] chars = folder.getBytes("UTF-8");
			byte[] b = new byte[255];
			b[0] = Jaraco.TYPE_FOLDER;
			for(int i=0;i<chars.length && (i + 1)<b.length;i++) {
				b[i + 1] = chars[i];
			}
			//b = ipEn.encrypt(b, key);
			Connector.socket.getOutputStream().write(b);
			
			fPosition = new Stack<Integer>();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		aaFolder = new FolderAdapter(this, alFolder);
		lvFolder.setAdapter(aaFolder);
		
		lvFolder.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				boolean reject = false;
				if(arg2 == select) {
					String file = alFolder.get(arg2);
					byte[] out = new byte[255];
					if(file.equals("d..")) {
						int l = folder.lastIndexOf("\\");
						if(l < 0) {
							l = folder.lastIndexOf("/");
						}
						file = folder.substring(0, l);
						folder = file;
						out[0] = Jaraco.TYPE_FOLDER;
						for(int i=1; i < out.length && i - 1 < file.length(); i++) {
							out[i] = (byte)file.charAt(i - 1);
						}
						if(fPosition.isEmpty()) {
							reject = true;
						} else {
							lPosition = fPosition.pop();
						}
					} else {
						folder = file.substring(1);
						byte[] f = null;
						try {
							f = file.getBytes("UTF-8");
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for(int i=1; i < out.length && i < f.length; i++) {
							out[i] = f[i];
						}
						fPosition.push(lvFolder.getFirstVisiblePosition());
						lPosition = 0;
					}
					
					if(file.charAt(0) == 'f') {
						
						out[0] = Jaraco.TYPE_ADDSONG;
					
					} else if(file.charAt(0) == 'd') {
						out[0] = Jaraco.TYPE_FOLDER;
					}
	
					try{
						if(!reject) {
							//out = ipEn.encrypt(out, key);
							Connector.socket.getOutputStream().write(out);
							if(file.charAt(0) == 'f') {
								finish();
							}
						}
					} catch(Exception ex) {
						
					}
				} else {
					select = arg2;
				}
			}
		});
		
	}
}
