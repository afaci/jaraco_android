package tk.baumi.jaraco;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
/**
 * The connector is for searching and selecting a player to connect.<br/>
 * It sends a broadcast to all clients of the network and lists all received answers.<br/>
 * If the network doesn't support broadcasts it is also possible to type-in the ip
 * automatically.
 * @author Manuel Baumgartner
 *
 */
public class Connector extends Activity {

	Button btConnect;
	Button btReload;
	EditText etIP;
	EditText etPort;
	static Socket socket;
	DatagramSocket udp;
	Context context;
	InetAddress group;
	ListView lvExplore;
	ProgressBar pbExplore;
	ArrayList<IPSelect> elem;
	ArrayAdapter<IPSelect> adt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connector);
		
		etIP = (EditText) findViewById(R.id.etIP);
		etIP.setText("192.168.43.135");
		etIP.clearFocus();
		
		etPort = (EditText) findViewById(R.id.etPort);
		etPort.setText("8800");
		
		btConnect = (Button) findViewById(R.id.btCon);
		btReload = (Button) findViewById(R.id.btReload);
		
		pbExplore = (ProgressBar) findViewById(R.id.pbExplore);
		
		lvExplore = (ListView) findViewById(R.id.lvExplore);
		elem = new ArrayList<IPSelect>();
		adt = new ArrayAdapter<IPSelect>(this,android.R.layout.simple_list_item_1,elem);
		lvExplore.setAdapter(adt);
		
		lvExplore.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				IPSelect ip = elem.get(arg2);
				etIP.setText(ip.getIp().getHostAddress());
			}
		});
		
		context = this;
		
		Explore ex = new Explore();
		ex.execute("");
		
		btReload.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Explore ex = new Explore();
				ex.execute("");
			}
		});
		
		btConnect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Connect con = new Connect();
				con.execute(
						etIP.getText().toString(),
						etPort.getText().toString()
						);
			}
		});
	}
	/**
	 * The IP-select-class is for listing the received answers by showing the hostname
	 * (computername).
	 * @author Manuel Baumgartner
	 *
	 */
	class IPSelect
	{
		private InetAddress ip;
		private String hostname;
		private int port;
		/**
		 * The only constructor is with the ip and the hostname.
		 * @param ip The InetAddress (ipv4 or ipv6)
		 * @param hostname The hostname as string (pc-name)
		 */
		public IPSelect(InetAddress ip, String hostname) {
			super();
			this.ip = ip;
			this.hostname = hostname;
		}
		/**
		 * For checking if an ip has already been added.
		 */
		@Override
		public boolean equals(Object o) {
			try {
				IPSelect other = (IPSelect)o;
				return this.ip.equals(other.ip);
			} catch (ClassCastException e) {
				return false;
			}
		}
		public InetAddress getIp() {
			return ip;
		}

		public String getHostname() {
			return hostname;
		}
		public void setHostname(String hostname) {
			this.hostname = hostname;
		}
		public int getPort() {
			return port;
		}
		public void setPort(int port) {
			this.port = port;
		}
		@Override
		public String toString()
		{
			super.toString();
			return hostname;
		}
	}
	/**
	 * The Explorer-AsyncTask sends an broadcasts and wait for responses.<br/>
	 * If the phone receives a response, the hostname with the ip will be added to
	 * the list.<br/>
	 * Due to bad network-conditions that occur sometimes, it sends multiple packages.
	 * @author Manuel Baumgartner
	 *
	 */
	class Explore extends AsyncTask<String, Object, Object>
	{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pbExplore.setVisibility(View.VISIBLE);
			btReload.setVisibility(View.GONE);
			try{
				WifiManager wifi = (WifiManager)getSystemService( Context.WIFI_SERVICE );
		        if(wifi != null)
		        {
		            WifiManager.MulticastLock lock = wifi.createMulticastLock("WifiDevices");
		            lock.acquire();
		        }
			} catch (Exception ex) {
				Toast.makeText(getBaseContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
			}
		}
		/**
		 * The progress-update will be executed if the phone receives a response.
		 */
		@Override
		protected void onProgressUpdate(Object...values) {
			super.onProgressUpdate(values);
			try {
				IPSelect ip = (IPSelect) values[0];
				boolean exists = false;
				for(int i = 0; i < elem.size() && !exists; i++) {
					if(elem.get(i).equals(ip)) {
						exists = true;
					}
				}
				if(!exists) {
					elem.add(ip);
					adt.notifyDataSetChanged();
				}
				pbExplore.setVisibility(View.GONE);
			} catch (Exception e) {
				
			}
		}
		/**
		 * In the current version, the global broadcast 255.255.255.255 is used.<br/>
		 * In the following version the local broadcast (e.g. 192.168.0.255) will be used.
		 * @return The local broadcast-address (e.g. 10.0.0.255)
		 * @throws IOException
		 */
		InetAddress getBroadcastAddress() throws IOException {
		    WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		    DhcpInfo dhcp = wifi.getDhcpInfo();
		    /*if(dhcp == null) {
		    	return InetAddress.getByName("255.255.255.255");
		    }*/
		    int netmask = 0xFFFFFF00;
		    //int ip = dhcp.ipAddress;
		    int or = 0xFFFFFFFF;
		    int broadcast = (dhcp.ipAddress & netmask) | or;
		    byte[] quads = new byte[4];
		    for (int k = 0; k < 4; k++)
		      quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
		    return InetAddress.getByAddress(quads);
		}
		
		@Override
		protected Object doInBackground(String... arg0) {
			try{
				udp = new DatagramSocket();
				 group = getBroadcastAddress();
				 
				 udp.setBroadcast(true);
				 
				byte[] b = new byte[]{'E','X','P','L','O','R','E', 'R'};
					
				DatagramPacket send = new DatagramPacket(b, b.length, group, 8802);
				udp.send(send);
				
				udp.setSoTimeout(1000);
				b = new byte[128];
				DatagramPacket receive = new DatagramPacket(b, b.length);
				boolean repeat = true;
				for(int i=0;i<10 && repeat;i++) {
					try{
						udp.receive(receive);
						publishProgress(new IPSelect(receive.getAddress(), new String(b)));
					} catch (IOException e) {
						repeat = true;
						udp.send(send);
					}
				}
				return null;
			} catch (Exception ex) {
				return "Exception "+ex.getMessage();
			}
		}
		/**
		 * If all packages has been sent and the receiver timed out the reload-button
		 * appears.<br/>
		 * If e.g. the wifi was deactivated an exception-message will be shown in a toast.
		 */
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			pbExplore.setVisibility(View.GONE);
			btReload.setVisibility(View.VISIBLE);
			if(result != null && result.getClass().equals(String.class)) {
				
				Toast.makeText(getApplicationContext(), (String)result,
						Toast.LENGTH_LONG).show();
			}
		}
	}
	/**
	 * The Connect-async-task actually starts a connection.
	 * @author Manuel Baumgartner
	 *
	 */
	class Connect extends AsyncTask<String, Void, Object>
	{
		ProgressDialog progress;
		
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			progress = new ProgressDialog(context);
			progress.setCancelable(true);
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setTitle("Connector");
			progress.setMessage("Establishing Connection...");
			
			progress.show();
		}
		@Override
		protected Object doInBackground(String... arg) {
			Socket s;
			try {
				s = new Socket();
				SocketAddress sa = new InetSocketAddress(arg[0], Integer.parseInt(arg[1]));
				s.connect(sa, 10000);
			} catch (NumberFormatException e) {
				return new String("Wrong number-format: "+e.getLocalizedMessage());
			} catch (UnknownHostException e) {
				return new String("Unknown host: "+e.getLocalizedMessage());
			} catch (IOException e) {
				return new String("IO-Exception: "+e.getLocalizedMessage());
			} catch (Exception e) {
				return new String("Exception: "+e.getLocalizedMessage());
			}
			return s;
		}
		
		@Override
		protected void onPostExecute(Object result)
		{
			super.onPostExecute(result);
			
			progress.dismiss();
			
			if(result.getClass().equals(Socket.class)) {
				socket = (Socket) result;
				
				Intent in = new Intent(context, Jaraco.class);
				startActivity(in);
			} else if (result.getClass().equals(String.class)) {
				AlertDialog.Builder adb = new AlertDialog.Builder(context);
				String error = (String) result;
				adb.setMessage(error);
				adb.setNegativeButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						arg0.cancel();
					}
				});
				
				adb.show();
			}
		}
		
	}

}
