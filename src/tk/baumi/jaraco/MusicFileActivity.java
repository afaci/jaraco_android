package tk.baumi.jaraco;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
/**
 * This activity shows all files to a specified album or artist.<br/>
 * By clicking on a song, the upload starts.
 * @author Manuel Baumgartner
 *
 */
public class MusicFileActivity extends Activity{

	ListView lvTitles;
	Context context;
	public MusicFileActivity() {
		// TODO Auto-generated constructor stub
	}
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_music_file);
		
		context = this;
		
		lvTitles = (ListView) findViewById(R.id.lvMusicTitle);
		
		byte mode = getIntent().getByteExtra("MODE", (byte) 0);
		String parameter = getIntent().getStringExtra("PARAM");
		
		String selection = null;
		if(mode == 1) {
			selection = MediaStore.Audio.Media.ARTIST + " = \"" + parameter + "\"";
		} else {
			selection = MediaStore.Audio.Media.ALBUM + " = \"" + parameter + "\"";
		}
		
		String[] projection = new String[] {
			MediaStore.Audio.Media._ID,
			MediaStore.Audio.Media.TITLE,
			MediaStore.Audio.Media.DATA
		};
		
		Cursor cur = getContentResolver().query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, 
				projection, 
				selection, 
				null, 
				MediaStore.Audio.Media.TRACK);
		
		final ArrayList<MusicFile> files = new ArrayList<MusicFile>();
		while(cur.moveToNext()) {
			MusicFile cMusic = new MusicFile();
			cMusic.setDisplay_name(cur.getString(1));
			cMusic.setLink(cur.getString(1));
			cMusic.setPath(cur.getString(2));
			
			files.add(cMusic);
		}
		
		ArrayAdapter<MusicFile> aaMusic = new 
				ArrayAdapter<MusicFile>(context, android.R.layout.simple_list_item_1,
						files);
		lvTitles.setAdapter(aaMusic);
		lvTitles.setCacheColorHint(0);
		lvTitles.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				MusicFile mf = files.get(arg2);
				FileUploader fu = new FileUploader(context);
				fu.execute(mf.getPath());
			}
		});
	}

}
