package tk.baumi.jaraco;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * The state-transfer receives all packages from the player and sends the
 * information via observer (listener).
 * @author Manuel Baumgartner
 *
 */
public class StateTransfer implements Runnable {
	/**
	 * The state has changed
	 * <ul>
	 * <li>changePlay     -- New file playing</li>
	 * <li>getPlaylist    -- New playlist</li>
	 * <li>extendPlaylist -- Add to playlist</li>
	 * </ul>
	 * @author Manuel Baumgartner
	 *
	 */
	public interface StateListener {
		public void changePlay(int listNum, String artist, String song, 
				boolean playing, int pos, int max);
		public void getPlaylist(ArrayList<String> list);
		public void extendPlaylist(ArrayList<String> list);
	}
	/**
	 * The folder-information for the FolderActivity
	 * <ul>
	 * <li>getFolder    -- Delete old list, add elements</li>
	 * <li>extendFolder -- Add elements to list</li>
	 * </ul>
	 * @author Manuel Baumgartner
	 *
	 */
	public interface FolderListener {
		public void getFolder(ArrayList<String> list);
		public void extendFolder(ArrayList<String> list);
	}
	private Socket access;
	private StateListener state;
	private FolderListener folder;
	private boolean running = true;
	/**
	 * The StateTransfer needs the socket for listening and the StateListener.
	 * @param socket The connected socket.
	 * @param state The StateListener
	 */
	public StateTransfer(Socket socket, StateListener state) {
		this.access = socket;
		this.state = state;
	}
	/**
	 * The FolderListener can be added afterwards from the FolderActivity.
	 * @param folder The FolderListener
	 */
	public void addFolderListener(FolderListener folder) {
		this.folder = folder;
	}

	@Override
	public void run() {
		while(access != null && access.isConnected() && !access.isClosed() && running) {
			try {
				byte[] buf = new byte[Jaraco.MTU];
				InputStream is = access.getInputStream();
				int read = is.read(buf);
				command(buf, read);
			} catch (Exception e) {
				try {
					access.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				access = null;
				e.printStackTrace();
			}
		}
	}
	/**
	 * Each command will be split into the operations.
	 * @param buf The buffer-array.
	 * @param read amout of read bytes.
	 */
	public void command(byte[] buf, int read) {
		//buf = besEncrypt.decrypt(buf, key);
		if(buf.length >= 8) {
			if(buf[0] == Jaraco.TYPE_POSITION) {
				/*
				if(buf[6] > 0)
				{
					tvPos.setText(buf[3]+ ":" + buf[2] + ":" + buf[1]);
					tvLen.setText(buf[6] + ":" + buf[5] + ":" + buf[4]);
				} else 
				{
					tvPos.setText(buf[2]+ ":" + buf[1]);
					tvLen.setText(buf[5] + ":" + buf[4]);
				}
				
				pbPosition.setMax(buf[6] * 3600 + buf[5] * 60 + buf[4]);
				pbPosition.setProgress(buf[3] * 3600 + buf[2] * 60 + buf[1]);
				*/
			} else if(buf[0] == Jaraco.TYPE_PLAYLIST) {
				try {
					ArrayList<String> playlist = new ArrayList<String>();
					String line = "";
					for(int i=1;i<buf.length && buf[i] != 0;i++) {
						char c = (char)buf[i];
						if(c == '\n') {
							playlist.add(line);
							line = "";
						} else {
							line += c;
						}
					}
					state.getPlaylist(playlist);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if(buf[0] == Jaraco.TYPE_PLAYLIST_ADD) {
				try {
					ArrayList<String> playlist = new ArrayList<String>();
					String line = "";
					for(int i=1;i<buf.length && buf[i] != 0;i++) {
						char c = (char)buf[i];
						if(c == '\n') {
							playlist.add(line);
							line = "";
						} else {
							line += c;
						}
					}
					state.extendPlaylist(playlist);
				} catch (Exception e) {
					
				}
			} else if(buf[0] == Jaraco.TYPE_SONGNAME) {
				try {
					String s = new String(buf,1,read - 1,"UTF-8");
					String[] ar = s.split("\n");
					int num = 0;
					try {
						num = Integer.parseInt(ar[0]);
					} catch (NumberFormatException e) {
						System.err.println("No current-number transmitted");
					}
					boolean playing = false;
					if(ar[3].equals("T")) {
						playing = true;
					}
					int pos = 0, max = 0;
					try {
						pos = Integer.parseInt(ar[4]);
						max = Integer.parseInt(ar[5]);
					} catch (NumberFormatException e) {
						System.err.println("No position transmitted");
					}
					if(ar.length >= 6) {
						state.changePlay(num, ar[1], ar[2], playing, pos, max);
					}
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if(buf[0] == Jaraco.TYPE_FOLDER) {
				try{
					ArrayList<String> list = new ArrayList<String>();
					list.add("d..");
					String s = new String(buf,1,buf.length - 1,"UTF-8");
					System.out.println(s);
					String[] sp = s.split("\n");
					for(int i=0;i<sp.length && !sp[i].equals("");i++) {
						if(sp[i].charAt(0) != 0) {
							list.add(sp[i]);
						}
					}
					if(folder != null) {
						folder.getFolder(list);
					}
				} catch (Exception e) {
					//Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			} else if(buf[0] == Jaraco.TYPE_FOLDER_ADD) {
				try{
					ArrayList<String> list = new ArrayList<String>();
					String s = new String(buf,1,buf.length - 1,"UTF-8");
					System.out.println(s);
					String[] sp = s.split("\n");
					for(int i=0;i<sp.length && !sp[i].equals("");i++) {
						if(sp[i].charAt(0) != 0) {
							list.add(sp[i]);
						}
					}
					if(folder != null) {
						folder.extendFolder(list);
					}
				} catch (Exception e) {
					
				}
			}
		}
	}

}
