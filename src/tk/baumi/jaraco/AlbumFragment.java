package tk.baumi.jaraco;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
/**
 * The AlbumFragment shows all albums from the music-library.<br/>
 * The user can select an album and sees all songs for this album.
 * @author Manuel Baumgartner
 *
 */
public class AlbumFragment extends Fragment {
	ListView lvAlbum;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_music, container, false);
		lvAlbum = (ListView) view.findViewById(android.R.id.list);
		
		final ArrayList<MusicFile> alAlbum = MusicFile.loadAlbum(getActivity());
		ArrayAdapter<MusicFile> aaAlbum = new ArrayAdapter<MusicFile>(getActivity(), android.R.layout.simple_list_item_1, alAlbum);
		lvAlbum.setAdapter(aaAlbum);
		lvAlbum.setCacheColorHint(0);
		lvAlbum.setFastScrollEnabled(true);
		
		lvAlbum.setOnItemClickListener(new ListView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				MusicFile music = alAlbum.get(arg2);
				//byte mode = getIntent().getByteExtra("MODE", (byte) 0);
				//String parameter = getIntent().getStringExtra("PARAM");
				Intent intent = new Intent(getActivity(), MusicFileActivity.class);
				intent.putExtra("MODE", (byte)2);
				intent.putExtra("PARAM", music.getLink());
				startActivity(intent);
			}
			
		});
		return view;
	}
}
