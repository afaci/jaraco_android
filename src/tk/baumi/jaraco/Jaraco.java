package tk.baumi.jaraco;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
/**
 * Jaraco is the main-activity, but it appears after the client has successfully connected<br/>
 * This activity shows
 * <ul>
 * <li>The current playlist with the votes</li>
 * <li>Control buttons</li>
 * 		<ul>
 * 		<li>Play</li>
 * 		<li>Previous file</li>
 * 		<li>Next file</li>
 * 		<li>Add a new file</li>
 * 		</ul>
 * <li>The position of the current file</li>
 * </ul>
 * @author Manuel Baumgartner
 *
 */
public class Jaraco extends Activity implements OnClickListener {
	public static short MTU = 1024;
	private ListView lvPlaylist;
	private OutputStream out;
	private ArrayAdapter<String> aaPlaylist;
	private Button btAdd, btPlay, btPrev, btNext;
	private TextView tvArtist, tvSong;
	private ArrayList<String> playlist;
	public static StateTransfer transfer;
	public static Thread stateChange;
	private TelephonyManager tm;
	private ProgressBar pbSong;
	private Context context;
	private Thread tProg;
	/**
	 * These constants are for splitting the frames into multiple operations.
	 */
	public static byte TYPE_POSITION = 1;
    public static byte TYPE_SONGNAME = 2;
    public static byte TYPE_PLAYLIST = 3;
    public static byte TYPE_PLAYLIST_ADD = 4;
    public static byte TYPE_ULSONG = 6;
    public static byte TYPE_ULSONG_ADD = 7;
    public static byte TYPE_FOLDER = 8;
    public static byte TYPE_FOLDER_ADD = 9;
    public static byte TYPE_DLSONG = 10;
    public static byte TYPE_DLSONG_ADD = 11;
    public static byte TYPE_VOLUME = 12;
    public static byte TYPE_ADDSONG = 13;
    public static byte TYPE_PHONE_STOP = 14;
    public static byte TYPE_ULSONG_END = 15;
    public static byte TYPE_VOTE = 16;
    
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		//Initializing GUI-elements
		tvArtist = (TextView) findViewById(R.id.tvMainCurrentArtist);
		tvSong = (TextView) findViewById(R.id.tvMainCurrentSong);
		
		btPlay = (Button) findViewById(R.id.btMainPlay);
		btNext = (Button) findViewById(R.id.btMainNext);
		btPrev = (Button) findViewById(R.id.btMainPrev);
		btAdd  = (Button) findViewById(R.id.btMainAdd);
		
		pbSong = (ProgressBar) findViewById(R.id.pbMainPosition);
		
		btPlay.setOnClickListener(this);
		btNext.setOnClickListener(this);
		btPrev.setOnClickListener(this);
		btAdd.setOnClickListener(this);
		
		context = this;
		try {
			out = Connector.socket.getOutputStream();
		} catch (IOException e) {
			out = null;
		}
		
		tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
		/**
		 * If the user is called by phone, a phone-stop request will be sent.
		 */
		tm.listen(new PhoneStateListener() {
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				if(state == TelephonyManager.CALL_STATE_RINGING) {
					byte[] buf = new byte[8];
					buf[0] = TYPE_PHONE_STOP;
					try {
						out.write(buf);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}, PhoneStateListener.LISTEN_CALL_STATE);
		
		lvPlaylist = (ListView) findViewById(R.id.lvMainPlaylist);
		lvPlaylist.setCacheColorHint(0);
		lvPlaylist.setOnItemClickListener(new ListView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				vote(arg2);
				
			}
			
		});
		playlist = new ArrayList<String>();
		
	
		
		aaPlaylist = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,playlist);
		lvPlaylist.setAdapter(aaPlaylist);
	
		transfer = new StateTransfer(Connector.socket, new StateTransfer.StateListener() {
			
			@Override
			public void getPlaylist(final ArrayList<String> list) {
				runOnUiThread(new Runnable() {
					public void run() {
						playlist.clear();
						for(int i = 0; i < list.size(); i++) {
							playlist.add(list.get(i));
							System.out.println(list.get(i));
						}
						aaPlaylist.notifyDataSetChanged();
					}
				});
			}
			
			@Override
			public void extendPlaylist(final ArrayList<String> list) {
				
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						playlist.addAll(list);
						aaPlaylist.notifyDataSetChanged();
					}
				});
				
			}
			
			@Override
			public void changePlay(int listNum, final String artist, final String song, final boolean playing ,final int pos, final int max) {
				runOnUiThread(new Runnable() {
					public void run() {
						tvArtist.setText(artist);
						tvSong.setText(song);
						//Toast.makeText(getApplicationContext(), pos + "/" + max, Toast.LENGTH_LONG).show();
					}
				});
			}
		});
		
		stateChange = new Thread(transfer);
		stateChange.start();
	}
	/**
	 * The add-dialog makes it possible to distinguish between local music on the phone
	 * and the music lying on the server.
	 */
	public void showAddDialog() {
		CharSequence[] elems = new CharSequence[]{
				"Local music", "Server music"
		};
		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		adb.setItems(elems, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Intent in = null;
				if(arg1 == 1) {
					in = new Intent(Jaraco.this, FolderActivity.class);
				} else {
					in = new Intent(Jaraco.this, LocalMusic.class);
				}
				startActivity(in);
			}
		});
		adb.show();
	}
	
	public void onDestroy() {
		super.onDestroy();
		try {
			Connector.socket.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * A vote of a song is represented with the IP and the id of the song.<br/>
	 * The IP is anyway given in a deeper-layer.
	 * @param songID The song-position in the playlist.<br/>
	 * It will be represented in a byte-array with four elements.
	 */
	public void vote(int songID) {
		byte[] b = IPEncryption.intToByteArray(songID);
		byte send[] = new byte[255];
		send[0] = TYPE_VOTE;
		send[1] = b[0];
		send[2] = b[1];
		send[3] = b[2];
		send[4] = b[3];
		try {
			out.write(send);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View arg0) {
		if(arg0.getId() == btPlay.getId()) {
			try {
				byte[] buf = new byte[8];
				buf[0] = 1;
				out.write(buf);
				if(tProg == null) {
					tProg = new Thread(new ProgressThread(0, 100));
					tProg.start();
				} else {
					tProg.interrupt();
					tProg = null;
				}
			} catch (Exception e) {
				finish();
			}
		} else if(arg0.getId() == btPrev.getId()) {
			try{
				byte[] buf = new byte[8];
				buf[0] = 3;
				//buf = besEncrypt.encrypt(buf, key);
				out.write(buf);
			} catch (Exception ex) {
				finish();
			}
		} else if(arg0.getId() == btNext.getId()) {
			try{
				
				byte[] buf = new byte[8];
				buf[0] = 4;
				//buf = besEncrypt.encrypt(buf, key);
				out.write(buf);
			} catch (Exception ex) {
				finish();
			}
		} else if(arg0.getId() == btAdd.getId()) {
			/*Intent in = new Intent(this, FolderActivity.class);
			startActivity(in);*/
			showAddDialog();
		}
	}
	/**
	 * The progress-thread adds seconds to the current progress.
	 * @author Manuel Baumgartner
	 *
	 */
	private class ProgressThread implements Runnable{
		int prog, max;
		boolean run;
		public ProgressThread(int prog, int max) {
			super();
			this.prog = prog;
			this.max = max;
			pbSong.setMax(max);
			run = true;
		}

		@Override
		public void run() {
			try {
				while(run && prog <= max) {
					pbSong.post(new Runnable() {
						
						@Override
						public void run() {
							pbSong.setProgress(prog);
						}
					});
					prog++;
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
				System.err.println("Interrupted");
			}
		}
		
	}
}
