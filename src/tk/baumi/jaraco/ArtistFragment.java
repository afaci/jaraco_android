package tk.baumi.jaraco;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
/**
 * The ArtistFragment shows all artists of songs from the music-library.<br/>
 * The user can select an artist and sees all songs that were made from this artist.
 * @author Manuel Baumgartner
 *
 */
public class ArtistFragment extends Fragment {
	ListView lvArtist;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_music, container, false);
		lvArtist = (ListView) view.findViewById(android.R.id.list);
		
		final ArrayList<MusicFile> alArtist = MusicFile.loadArtist(getActivity());
		ArrayAdapter<MusicFile> aaArtist = new ArrayAdapter<MusicFile>(getActivity(), android.R.layout.simple_list_item_1, alArtist);
		lvArtist.setAdapter(aaArtist);
		lvArtist.setCacheColorHint(0);
		lvArtist.setFastScrollEnabled(true);
		
		lvArtist.setOnItemClickListener(new ListView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				MusicFile music = alArtist.get(arg2);
				//byte mode = getIntent().getByteExtra("MODE", (byte) 0);
				//String parameter = getIntent().getStringExtra("PARAM");
				Intent intent = new Intent(getActivity(), MusicFileActivity.class);
				intent.putExtra("MODE", (byte)1);
				intent.putExtra("PARAM", music.getLink());
				startActivity(intent);
			}
			
		});
		return view;
	}

}
