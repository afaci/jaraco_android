package tk.baumi.jaraco;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
/**
 * The MusicFragmentManager provides the Fragments for the LocalMusicActivity.
 * @author Manuel Baumgartner
 *
 */
public class MusicFragmentManager extends FragmentPagerAdapter {

	private Fragment fragments[];
	public MusicFragmentManager(FragmentManager fm) {
		super(fm);
		fragments = new Fragment[] {
				new MusicFragment(),
				new ArtistFragment(),
				new AlbumFragment()};
	}

	@Override
	public Fragment getItem(int arg0) {
		return fragments[arg0];
	}

	@Override
	public int getCount() {
		return fragments.length;
	}

}
