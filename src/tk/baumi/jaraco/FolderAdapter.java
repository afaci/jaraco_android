package tk.baumi.jaraco;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * The FolderAdapter is an arrayadapter.<br/>
 * Folders are identicated with a folder-icon and files with a file-icon.
 * @author Afaci
 *
 */
public class FolderAdapter extends ArrayAdapter<String> {

	private final Context context;
	private final ArrayList<String> values;
 
	public FolderAdapter(Context context, ArrayList<String> values) {
		super(context, R.layout.filelistview, values);
		this.context = context;
		this.values = values;
		
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = null;
		if(convertView != null) {
			rowView = convertView;
		} else {
			rowView = inflater.inflate(R.layout.filelistview, parent, false);
		}
 
		TextView textView = (TextView) rowView.findViewById(R.id.tvFolder);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.imFolder);
		// Change icon based on name
		String s = values.get(position);
		char type = 'f';
		String file = "";
		try {
		type = s.charAt(0);
		file = s.substring(1);
		} catch (Exception e) {
			
		}
		
		int l = file.lastIndexOf("\\");
		if(l < 0) {
			l = file.lastIndexOf("/");
		}
		file = file.substring(l + 1);
		
		//int lExt = file.lastIndexOf(".");
		//String ext = file.substring(lExt + 1);
		
		textView.setText(file);
		
		if(type == 'd') {
			Picasso.with(context).load(R.drawable.folder).into(imageView);
		} else {
			Picasso.with(context).load(R.drawable.file).into(imageView);
		}
		
		return rowView;
	}

}
