package tk.baumi.jaraco;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
/**
 * The local-music is split into three categories.
 * <ul>
 * <li>Song   (Lists all songs)</li>
 * <li>Artist (Lists all artists)</li>
 * <li>Album  (Lists all albums</li>
 * @author Manuel Baumgartner
 *
 */
public class LocalMusic extends FragmentActivity implements OnPageChangeListener, OnTabChangeListener  {
	private TabHost mTabHost;
	private ViewPager pager;
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_local_music);
		
		MusicFragmentManager pageAdapter = new MusicFragmentManager(getSupportFragmentManager());
		pager = (ViewPager)findViewById(R.id.viewpager);
		pager.setAdapter(pageAdapter);
		
		pager.setOnPageChangeListener(this);
		
		initialiseTabHost();
	}
	/**
	 * The tab-factory returns an empty-tab.
	 * @author Manuel Baumgartner
	 *
	 */
	class TabFactory implements TabContentFactory {
		 
        private final Context mContext;
 
        /**
         * @param context
         */
        public TabFactory(Context context) {
            mContext = context;
        }
 
        /** (non-Javadoc)
         * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
         */
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
 
    }
	/**
	 * The tabhost has the three elements<br/>
	 * Music, Artists and Album.<br/>
	 * The original representation is shown in fragments.<br/>
	 * The tabs are just for switching between them.
	 */
	private void initialiseTabHost() {
        mTabHost = (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup();
        
        // Default to first tab
        //this.onTabChanged("Tab1");
        //
        TabSpec ts1 = mTabHost.newTabSpec("Music");
        ts1.setContent(new TabFactory(this));
        ts1.setIndicator("Music", getResources().getDrawable(R.drawable.music));
        
        TabSpec ts2 = mTabHost.newTabSpec("Artist");
        ts2.setContent(new TabFactory(this));
        ts2.setIndicator("Artist", getResources().getDrawable(R.drawable.artist));
        
        TabSpec ts3 = mTabHost.newTabSpec("Album");
        ts3.setContent(new TabFactory(this));
        ts3.setIndicator("Album", getResources().getDrawable(R.drawable.album));
        
        mTabHost.addTab(ts1);
        mTabHost.addTab(ts2);
        mTabHost.addTab(ts3);
        
        mTabHost.setOnTabChangedListener(this);
    }
	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onPageSelected(int arg0) {
		mTabHost.setCurrentTab(arg0);
	}
	@Override
	public void onTabChanged(String arg0) {
		pager.setCurrentItem(mTabHost.getCurrentTab());
	}
}
