package tk.baumi.jaraco;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
/**
 * The FileUploader task uploads a file and shows the progress in a progress-bar<br/>
 * In the current version all other controls are locked while uploading a file.<br/>
 * The upload is also cancelable.
 * @author Manuel Baumgartner
 *
 */
public class FileUploader extends AsyncTask<String, Integer, String> {

	OutputStream os;
	String filename;
	byte[] buf;
	Context context;
	ProgressDialog progress;
	boolean cancel = false;
	
	public FileUploader(Context context) {
		this.context = context;
		cancel = false;
	}
	protected void onPreExecute() {
		super.onPreExecute();
		
		try {
			os = Connector.socket.getOutputStream();
			Connector.socket.setTcpNoDelay(true);
			Connector.socket.setSendBufferSize(Jaraco.MTU);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		progress = new ProgressDialog(context);
		progress.setTitle("Uploading file...");
		progress.setMessage("please wait");
		progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progress.setCancelable(false);
		progress.setButton(ProgressDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				cancel = true;
			}
		});
		progress.show();
	}
	
	protected void onProgressUpdate(Integer...values) {
		super.onProgressUpdate(values);
		if(values.length >= 2) {
			progress.setMax(values[1]);
			progress.setProgress(values[0]);
		}
	}
	@Override
	protected String doInBackground(String... arg) {
		filename = arg[0];
		File f = new File(filename);
		if(f.exists()) {
			buf = new byte[Jaraco.MTU];
			try {
				buf = filename.getBytes();
				buf[0] = Jaraco.TYPE_ULSONG;
				os.write(buf);
				os.flush();
				int sent = 0;
				
				BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename));
				//DataInputStream is = new DataInputStream(new FileInputStream(filename));
				publishProgress(0, (int)f.length());
				int length = 0;
				buf = new byte[Jaraco.MTU];
				while((length = is.read(buf)) > 0 && !cancel) {
					sent += length;
					publishProgress(sent, (int)f.length());
					os.write(buf, 0, length);
					os.flush();
				}
				os.flush();
				is.close();
				Connector.socket.setSendBufferSize(1);
				if(cancel) {
					buf = "ABORTTRANSMISS".getBytes("UTF-8");
				} else {
					buf = "ENDOFMUSICFILE".getBytes("UTF-8");
				}
				os.write(buf);
				os.flush();
				Connector.socket.setSendBufferSize(Jaraco.MTU);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		progress.dismiss();
	}

}
