package tk.baumi.jaraco;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
/**
 * The music-file is a representer for an ArrayAdapter.<br/>
 * It saves the link of the file (e.g. author-name), the display_name and the path.
 * @author Manuel Baumgartner
 *
 */
public class MusicFile implements Comparable<MusicFile>{
	private String link;
	private String display_name;
	private String path;
	public final static byte SHOW_TITLE = 0;
	public final static byte SHOW_ARTIST = 1;
	public final static byte SHOW_ALBUM = 2;
	
	public MusicFile() {
		// TODO Auto-generated constructor stub
	}
	

	public MusicFile(String _link, String _display_name, String _path) {
		super();
		link = _link;
		display_name = _display_name;
		path = _path;
	}

	


	public String getLink() {
		return link;
	}


	public void setLink(String link) {
		this.link = link;
	}


	public String getDisplay_name() {
		return display_name;
	}


	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	@Override
	public String toString() {
		return display_name;
	}

	@Override
	public int compareTo(MusicFile another) {
		return this.display_name.compareTo(another.display_name);
	}
	/**
	 * Loads the complete album-list.
	 * @param context
	 * @return The album-list as ArrayList of Strings.
	 */
	public static ArrayList<MusicFile> loadAlbum(Context context) {
		ArrayList<MusicFile> album = new ArrayList<MusicFile>();
		//Some audio may be explicitly marked as not being music
		String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
		
		String[] projection = {
				"DISTINCT " + MediaStore.Audio.Media.ALBUM
		};
		Cursor cursor = context.getContentResolver().query(
		        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
		        projection,
		        selection,
		        null,
		        MediaStore.Audio.Media.ALBUM);

		while(cursor.moveToNext()){
			album.add(new MusicFile(cursor.getString(0), cursor.getString(0), null));
		}
		return album;
	}
	/**
	 * Loads the complete artist-list.
	 * @param context
	 * @return The artist-list as ArrayList of Strings.
	 */
	public static ArrayList<MusicFile> loadArtist(Context context) {
		ArrayList<MusicFile> artists = new ArrayList<MusicFile>();
		//Some audio may be explicitly marked as not being music
		String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
		
		String[] projection = {
				"DISTINCT "+ MediaStore.Audio.Media.ARTIST
		};
		Cursor cursor = context.getContentResolver().query(
		        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
		        projection,
		        selection,
		        null,
		        MediaStore.Audio.Media.ARTIST);
		artists.ensureCapacity(cursor.getCount());
		while(cursor.moveToNext()){
			artists.add(new MusicFile(cursor.getString(0), cursor.getString(0), null));
		}
		return artists;
	}
	/**
	 * Loads the complete song-list.
	 * @param context
	 * @return The song-list as ArrayList of Strings.
	 */
	public static ArrayList<MusicFile> loadList(Context context) {
		ArrayList<MusicFile> s = new ArrayList<MusicFile>();
		//Some audio may be explicitly marked as not being music
		String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
		
		String[] projection = {
		        MediaStore.Audio.Media.TITLE,
		        MediaStore.Audio.Media.DATA
		};
		Cursor cursor = context.getContentResolver().query(
		        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
		        projection,
		        selection,
		        null,
		        MediaStore.Audio.Media.DISPLAY_NAME);

		while(cursor.moveToNext()){
			s.add(new MusicFile(null, cursor.getString(0), cursor.getString(1)));
		       // songs.add(cursor.getString(0) + "||" + cursor.getString(1) + "||" +   cursor.getString(2) + "||" +   cursor.getString(3) + "||" +  cursor.getString(4) + "||" +  cursor.getString(5));
		}
		
		return s;
	}
}
