package tk.baumi.jaraco;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * This fragment of the local_music activity shows all songs of the phone.<br/>
 * By clicking on a list-item it starts the upload.
 * @author Manuel Baumgartner
 *
 */
public class MusicFragment extends Fragment {
	
	private ListView lvMusic;
	public MusicFragment() {
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_music, container, false);
		lvMusic = (ListView) view.findViewById(android.R.id.list);
		
		final ArrayList<MusicFile> alMusic = MusicFile.loadList(getActivity());
		ArrayAdapter<MusicFile> aaMusic = new ArrayAdapter<MusicFile>(getActivity(), android.R.layout.simple_list_item_1, alMusic);
		lvMusic.setAdapter(aaMusic);
		lvMusic.setCacheColorHint(0);
		lvMusic.setFastScrollEnabled(true);
		
		lvMusic.setOnItemClickListener(new ListView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				MusicFile music = alMusic.get(arg2);
				FileUploader fu = new FileUploader(getActivity());
				fu.execute(music.getPath());
			}
			
		});
		return view;
	}

}
